package Run;

import data.DataParam;
import data.ResultTestCase;
import data.TestCase;
import object.BaseTest;


import org.testng.ITestContext;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import utils.APIController;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class RunTest extends BaseTest {
    ArrayList<ResultTestCase> resultTestCases = new ArrayList<>();
    String responseCon = null;
    Boolean resultCon = false;
    int statusColorTabCon = 0;
    int totalFailCaseCon = 0;
    int totalPassCaseCon = 0;

    @Parameters({"diaChiFile", "diaChiFileOutput"})
    @Test
    public void run(String diaChiFile, String diaChiFileOutput) {
        //String diaChiFile = "src/test/java/Run/Input/data_promotion_staging.xlsx";
        //String diaChiFileOutput = "src/test/java/Run/Output/data_promotion_staging_output.xlsx";
        String tenSheet = "ZMA_OG";
        String response = null;
        ArrayList<DataParam> resultParam = new ArrayList<DataParam>();
        Boolean result = false;
        TestCase[] testcases = null;

        ArrayList<TestCase> testCaseList = new ArrayList<>();
        testcases = docFileExcel(diaChiFile, tenSheet);
        BaseTest bt = new BaseTest();
        if (testcases != null) {
            for (int i = 0; i < testcases.length; i++) {
                System.out.println("Test case thứ " + (i + 1) + " là :" + testcases[i].getRefs());
                if (checkParam(testcases[i], "TESTCASE")) {// Nếu có chứa TESTCASE
                    //Hàm trả ra danh sách param sau khi đã giải mã
                    resultParam = hamCheckParam(testcases[i], resultTestCases, i);
                    //Hàm kiểm tra danh sách param trả ra
                    if(hamKiemTraResultParam(resultParam)){
                        testCaseList = layDSTestCaseCon(testcases[i], resultParam);
                        if (testCaseList != null) {
                            int resultCuaTCCon = 0;
                            for (int x = 0; x < testCaseList.size(); x++) {
                                resultCuaTCCon = chayTestCaseCon(testCaseList.get(x));
                            }
                            if(resultCuaTCCon >0){
                                testcases[i].setResult("FALSE");
                            }else{
                                testcases[i].setResult("PASSED");
                            }
                            if (testCaseList.size() == 1) {
                                testcases[i] = testCaseList.get(0);
                            } else {
                                testcases[i].setActualResponse("Xem test report ở sheet " + testcases[i].getRefs());
                                ghiFileExcelCon(testCaseList, testcases[i].getRefs(), diaChiFileOutput, statusColorTabCon);
                            }

                        }
                    }
                }
                else {
                    response = APIController.hamChayAPI(testcases[i]);
                    // System.out.println("Kết quả trả về là: " + response);
                    // Nếu response trả về khác rỗng, khác null, thì kiểm tra status code và
                    // expected Result
                    if (response != null && !response.equals("")) {
                        ResultTestCase rs = new ResultTestCase(testcases[i], response);

                        resultTestCases.add(rs);
                        result = hamKiemTraKetQua(response, testcases[i]);
                        if(testcases[i].getActualResponse()==null||testcases[i].getActualResponse().equals("")) {//Nếu API ko có test case con

                            testcases[i].setActualResponse(message);
                            if (result == false) {
                                totalFailCase++;
                                testcases[i].setResult("FALSE");
                            } else {
                                totalPassCase++;
                                testcases[i].setResult("PASSED");
                            }
                            System.out.println("Kết quả của test case là: " + result);
                            System.out.println(bt.getMessage());
                        }

                    }
                }
            }
        } else {
            System.out.println("Không có dữ liệu trong file");
        }
        if (testcases != null) {
            ghiFileExcel(testcases, diaChiFileOutput);
        }
    }


    private boolean hamKiemTraResultParam(ArrayList<DataParam> resultParam) {
        if(resultParam!=null){
            for(int i=0;i<resultParam.size();i++){
                if(resultParam.get(i).getKetqua()==false){
                    System.out.println("Không tìm thấy kết quả của param ở vị trí: "+ resultParam.get(i).getViTri());
                    return false;
                }
            }
        }
        return true;
    }
    @Parameters ({"diaChiFileOutput"})
    @AfterTest
    public void afterTest(ITestContext testContext, String diaChiFileOutput) {
        System.out.println("testContext: ===================  "+ testContext);
        sendNotificationHangsOut(testContext,diaChiFileOutput);
    }

    public Boolean checkParam(TestCase testCase, String keyWord) {
        if (testCase.getEndPoint().contains(keyWord) || testCase.getBody().contains(keyWord)
                || testCase.getBody().contains(keyWord) || testCase.getExpectedResult().contains(keyWord)) {
            return true;
        }
        return false;
    }

    public int chayTestCaseCon(TestCase testCase) {
    int result = 0;
        responseCon = APIController.hamChayAPI(testCase);
        statusColorTabCon =0;
        totalFailCaseCon =0;
        totalFailCaseCon =0;
        // System.out.println("Kết quả trả về là: " + response);
        // Nếu response trả về khác rỗng, khác null, thì kiểm tra status code và
        // expected Result
        if (responseCon != null && !responseCon.equals("")) {

            ResultTestCase rs = new ResultTestCase(testCase, responseCon);
            resultTestCases.add(rs);
            resultCon = hamKiemTraKetQua(responseCon, testCase);
            testCase.setActualResponse(message);
            if (resultCon == false) {
                statusColorTabCon++;
                totalFailCaseCon++;
                result++;
                testCase.setResult("FALSE");
            } else {
                totalPassCaseCon++;
                testCase.setResult("PASSED");
            }
            System.out.println("Kết quả của test case là: " + resultCon);
            System.out.println(getMessage());

        }
        return result;
    }

    public ArrayList<TestCase> layDSTestCaseCon(TestCase testCase, ArrayList<DataParam> resultParam) {
        ArrayList<TestCase> results = new ArrayList<>();
        if (resultParam != null) {
            ArrayList<DataParam> resultsNotNull = (ArrayList<DataParam>) resultParam.stream()
                    .filter(line -> line.getDataList()!=null)
                    .collect(Collectors.toList());

            switch (resultsNotNull.size()){
                case 1:{
                    results = (ArrayList<TestCase>) layTestCase1Param(resultsNotNull,testCase);
                    break;
                }
                case 2:{
                    results = (ArrayList<TestCase>) layTestCase2Param(resultsNotNull,testCase);
                    break;
                }
                case 3:{
                    results = (ArrayList<TestCase>) layTestCase3Param(resultsNotNull,testCase);
                    break;
                }
                case 4:{
                    results = (ArrayList<TestCase>) layTestCase4Param(resultsNotNull,testCase);
                    break;
                }
                default:break;
            }
            System.out.println("Chạy xong rồi");
            return results;

        }
        results.add(testCase);
        return results;
    }
    private List<TestCase> layTestCase1Param(ArrayList<DataParam> dataParams, TestCase testCase) {
        ArrayList<TestCase> results = new ArrayList<>();
        dataParams.get(0).getDataList().forEach(dataLine ->{
            TestCase testCase1 = hamTaoTestCase(testCase, dataParams.get(0).getViTri(), dataLine);
            results.add(new TestCase(testCase1.getRefs(), testCase1.getDescription(), testCase1.getEndPoint(), testCase1.getMethod(), testCase1.getHeader(), testCase1.getToken(), testCase1.getBody(), testCase1.getStatusCode(), testCase1.getExpectedResult(), testCase1.getActualResponse(), testCase1.getResult()));
        });
        return results;

    }
    private List<TestCase> layTestCase2Param(ArrayList<DataParam> dataParams, TestCase testCase) {
        ArrayList<TestCase> results = new ArrayList<>();
        dataParams.get(0).getDataList().forEach(dataLine1 -> {
            dataParams.get(1).getDataList().forEach(dataLine2 -> {
                TestCase testCase1 = hamTaoTestCase(testCase, dataParams.get(0).getViTri(), dataLine1);
                testCase1 = hamTaoTestCase(testCase1, dataParams.get(1).getViTri(), dataLine2);
                results.add(new TestCase(testCase1.getRefs(), testCase1.getDescription(), testCase1.getEndPoint(), testCase1.getMethod(), testCase1.getHeader(), testCase1.getToken(), testCase1.getBody(), testCase1.getStatusCode(), testCase1.getExpectedResult(), testCase1.getActualResponse(), testCase1.getResult()));
            });
        });
        return results;
    }
    private List<TestCase> layTestCase3Param(ArrayList<DataParam> dataParams, TestCase testCase) {
        ArrayList<TestCase> results = new ArrayList<>();
        dataParams.get(0).getDataList().forEach(dataLine1 -> {
            dataParams.get(1).getDataList().forEach(dataLine2 -> {
                dataParams.get(2).getDataList().forEach(dataLine3 -> {
                    TestCase testCase1 = hamTaoTestCase(testCase, dataParams.get(0).getViTri(), dataLine1);
                    testCase1 = hamTaoTestCase(testCase1, dataParams.get(1).getViTri(), dataLine2);
                    testCase1 = hamTaoTestCase(testCase1, dataParams.get(2).getViTri(), dataLine3);
                    results.add(new TestCase(testCase1.getRefs(), testCase1.getDescription(), testCase1.getEndPoint(), testCase1.getMethod(), testCase1.getHeader(), testCase1.getToken(), testCase1.getBody(), testCase1.getStatusCode(), testCase1.getExpectedResult(), testCase1.getActualResponse(), testCase1.getResult()));
                });
            });
        });
        return results;
    }
    private List<TestCase> layTestCase4Param(ArrayList<DataParam> dataParams, TestCase testCase) {
        ArrayList<TestCase> results = new ArrayList<>();
        dataParams.get(0).getDataList().forEach(dataLine1 -> {
            dataParams.get(1).getDataList().forEach(dataLine2 -> {
                dataParams.get(2).getDataList().forEach(dataLine3 -> {
                    dataParams.get(3).getDataList().forEach(dataLine4 -> {
                        TestCase testCase1 = hamTaoTestCase(testCase, dataParams.get(0).getViTri(), dataLine1);
                        testCase1 = hamTaoTestCase(testCase1, dataParams.get(1).getViTri(), dataLine2);
                        testCase1 = hamTaoTestCase(testCase1, dataParams.get(2).getViTri(), dataLine3);
                        testCase1 = hamTaoTestCase(testCase1, dataParams.get(3).getViTri(), dataLine4);
                        results.add(new TestCase(testCase1.getRefs(), testCase1.getDescription(), testCase1.getEndPoint(), testCase1.getMethod(), testCase1.getHeader(), testCase1.getToken(), testCase1.getBody(), testCase1.getStatusCode(), testCase1.getExpectedResult(), testCase1.getActualResponse(), testCase1.getResult()));
                    });
                });
            });
        });
        return results;
    }

    // @Test
    public void main() {
        TestCase testCase = new TestCase("TC01","Test case example","","POST","","","body","statusCode","","ActualResponse","result");
        TestCase testCase2 = new TestCase();
        ArrayList<String> list01 = new ArrayList<>();
        list01.add("A01");
        list01.add("A02");
        list01.add("A03");
        list01.add("A04");
        ArrayList<String> list02 = new ArrayList<>();
        list02.add("header01");
        list02.add("header02");
        ArrayList<String> list03 = new ArrayList<>();
        list03.add("ex01");
         ArrayList<String> list04 = new ArrayList<>();
         list04.add("body01");
         list04.add("body02");
        ArrayList<DataParam> dataParams = new ArrayList<>();
        DataParam re1 = new DataParam("TC01", "param01", "Value01", "EndPoint","", list01);
        DataParam re2 = new DataParam("TC02", "param01", "Value01", "Header","header02", list02);
        DataParam re3 = new DataParam("TC03", "param01", "Value01", "ExpectedResult","data03",list03);
        DataParam re4 = new DataParam("TC03", "param01", "Value01", "Body","data03",list04);

         dataParams.add(re1);
        dataParams.add(re2);
        dataParams.add(re3);
        dataParams.add(re4);
        ArrayList<TestCase> results = new ArrayList<>();
        ArrayList<DataParam> resultsNotNull = (ArrayList<DataParam>) dataParams.stream()
                .filter(line -> line.getDataList()!=null)
                .collect(Collectors.toList());

        switch (resultsNotNull.size()) {
            case 1: {
                results = (ArrayList<TestCase>) layTestCase1Param(resultsNotNull, testCase);
                break;

            }
            case 2: {
                results = (ArrayList<TestCase>) layTestCase2Param(resultsNotNull, testCase);
                break;

            }
            case 3: {
                results = (ArrayList<TestCase>) layTestCase3Param(resultsNotNull, testCase);
                break;

            }
            case 4: {
                results = (ArrayList<TestCase>) layTestCase4Param(resultsNotNull, testCase);
                break;

            }
        }
        System.out.println("Chạy xong rồi");

    }
    public TestCase hamTaoTestCase(TestCase testCase, String viTri, String dataLine) {
        TestCase result = new TestCase();
        result = testCase;
        result.setViTri(viTri,dataLine);
        return result;
    }





}