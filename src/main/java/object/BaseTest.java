package object;
import data.DataParam;
import data.ResultTestCase;
import data.TestCase;
import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.testng.ITestContext;
import com.jayway.jsonpath.Configuration;
import com.jayway.jsonpath.JsonPath;
import com.jayway.jsonpath.PathNotFoundException;
import org.apache.commons.lang.StringUtils;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.*;

import java.awt.Color;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import utils.GoogleDrive;
import utils.GoogleHangouts;


public class BaseTest {
    public static int totalFailCase = 0;
    public static int totalPassCase = 0;
    public XSSFSheet sheet;
    public XSSFWorkbook wb;
    protected String sheetName;
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String message = "";

    //Hàm đọc file test case
    public TestCase[] docFileExcel (String diaChiFile, String tenSheet){
        try {
            FileInputStream fileInputStream = new FileInputStream(diaChiFile);
            wb = new XSSFWorkbook(fileInputStream);
            //DataFormatter dataFormatter = new DataFormatter();
            sheet = wb.getSheet(tenSheet);
            TestCase[] testcases = null;
            if (sheet != null) {
                testcases = new TestCase[sheet.getLastRowNum()];
                System.out.println("Số dòng của file là: " + sheet.getLastRowNum());
                String dk = null;
                for (int i = 0; i <= sheet.getLastRowNum(); i++) {
                    if(i!=sheet.getLastRowNum()) {
                        testcases[i] = new TestCase();
                    }
                    for (int j = 0; j < sheet.getRow(0).getLastCellNum(); j++) {
                        dk = sheet.getRow(0).getCell(j).toString();
                        if (i != 0) {// Nếu dòng khác header thì lưu

                            switch (dk) {
                                case "Refs":
                                    testcases[i-1].setRefs(sheet.getRow(i).getCell(j).toString());
                                    break;
                                case "Testcase":
                                    testcases[i - 1].setDescription(sheet.getRow(i).getCell(j).toString());
                                    break;
                                case "EndPoint":
                                    testcases[i - 1].setEndPoint(sheet.getRow(i).getCell(j).toString());
                                    break;
                                case "Method":
                                    testcases[i - 1].setMethod(sheet.getRow(i).getCell(j).toString());
                                    break;
                                case "Header":
                                    testcases[i - 1].setHeader(sheet.getRow(i).getCell(j).toString());
                                    break;
                                case "Token":
                                    testcases[i - 1].setToken(sheet.getRow(i).getCell(j).toString());
                                    break;
                                case "Body":
                                    testcases[i - 1].setBody(sheet.getRow(i).getCell(j).toString());
                                    break;
                                case "StatusCode":
                                    testcases[i - 1].setStatusCode(sheet.getRow(i).getCell(j).toString());
                                    break;
                                case "ExpectedResult":
                                    testcases[i - 1].setExpectedResult(sheet.getRow(i).getCell(j).toString());
                                    break;
                                case "Actual Response":
                                    testcases[i - 1].setActualResponse(sheet.getRow(i).getCell(j).toString());
                                    break;
                                case "Result":
                                    testcases[i - 1].setResult(sheet.getRow(i).getCell(j).toString());
                                    break;
                                default:
                                    break;
                            }

                        }
                    }

                }
                fileInputStream.close();
                //wb.close();
                return testcases;

            }else{
                fileInputStream.close();
                //wb.close();
                System.out.println("Không tìm thấy sheet");
                return null;
            }
        }catch (NullPointerException npointer) {
            npointer.printStackTrace();
            System.out.println("Cannot open the file because the current file path is null.");
        } catch (FileNotFoundException nfound){
            nfound.printStackTrace();
            System.out.println("Didn't find the file " + diaChiFile + ". Please check file path or file name.");
        } catch (IOException io){
            io.printStackTrace();
        }
        return null;
    }
    //Hàm đọc full file test case
    public TestCase[] docFullFileExcel (String diaChiFile, String tenSheet){
        try {
            FileInputStream fileInputStream = new FileInputStream(diaChiFile);
            wb = new XSSFWorkbook(fileInputStream);
            //DataFormatter dataFormatter = new DataFormatter();
            XSSFSheet sheetFull = wb.getSheet(tenSheet);
            TestCase[] testcases = null;
            if (sheetFull != null) {
                testcases = new TestCase[sheetFull.getLastRowNum()+1];
                System.out.println("Số dòng của file là: " + (sheetFull.getLastRowNum()+1));
                String dk = null;
                for (int i = 0; i <= sheetFull.getLastRowNum(); i++) {
                    testcases[i] = new TestCase();
                    for (int j = 0; j < sheetFull.getRow(0).getLastCellNum(); j++) {
                        dk = sheetFull.getRow(0).getCell(j).toString();
                        // if (i != 0) {// Nếu dòng khác header thì lưu

                        switch (dk) {
                            case "Refs":
                                testcases[i].setRefs(sheetFull.getRow(i).getCell(j).toString());
                                break;
                            case "Testcase":
                                testcases[i].setDescription(sheetFull.getRow(i).getCell(j).toString());
                                break;
                            case "EndPoint":
                                testcases[i].setEndPoint(sheetFull.getRow(i).getCell(j).toString());
                                break;
                            case "Method":
                                testcases[i].setMethod(sheetFull.getRow(i).getCell(j).toString());
                                break;
                            case "Header":
                                testcases[i].setHeader(sheetFull.getRow(i).getCell(j).toString());
                                break;
                            case "Token":
                                testcases[i].setToken(sheetFull.getRow(i).getCell(j).toString());
                                break;
                            case "Body":
                                testcases[i].setBody(sheetFull.getRow(i).getCell(j).toString());
                                break;
                            case "StatusCode":
                                testcases[i].setStatusCode(sheetFull.getRow(i).getCell(j).toString());
                                break;
                            case "ExpectedResult":
                                testcases[i].setExpectedResult(sheetFull.getRow(i).getCell(j).toString());
                                break;
                            case "Actual Response":
                                testcases[i].setActualResponse(sheetFull.getRow(i).getCell(j).toString());
                                break;
                            case "Result":
                                testcases[i].setResult(sheetFull.getRow(i).getCell(j).toString());
                                break;
                            default:
                                break;
                        }


                    }

                }
                fileInputStream.close();
                //wb.close();
                return testcases;

            }else{
                fileInputStream.close();
                //wb.close();
                System.out.println("Không tìm thấy sheet");
                return null;
            }
        }catch (NullPointerException npointer) {
            npointer.printStackTrace();
            System.out.println("Cannot open the file because the current file path is null.");
        } catch (FileNotFoundException nfound){
            nfound.printStackTrace();
            System.out.println("Didn't find the file " + diaChiFile + ". Please check file path or file name.");
        } catch (IOException io){
            io.printStackTrace();
        }
        return null;
    }

    //Hàm ghi file Excel report
    public void ghiFileExcel (TestCase[] testCase,String diaChiFile){
        try {
            int statusColorTab =0;
            FileOutputStream fileOutputStream = new FileOutputStream(diaChiFile);
            XSSFRow row = null;
            for (int i = 0; i < testCase.length; i++) {
                row = sheet.createRow((short) i + 1);
                row.createCell(0).setCellValue(testCase[i].getRefs());
                row.createCell(1).setCellValue(testCase[i].getDescription());
                row.createCell(2).setCellValue(testCase[i].getEndPoint());
                row.createCell(3).setCellValue(testCase[i].getMethod());
                row.createCell(4).setCellValue(testCase[i].getHeader());
                row.createCell(5).setCellValue(testCase[i].getToken());
                row.createCell(6).setCellValue(testCase[i].getBody());
                row.createCell(7).setCellValue(testCase[i].getStatusCode());
                row.createCell(8).setCellValue(testCase[i].getExpectedResult());
                row.createCell(9).setCellValue(StringUtils.substring(testCase[i].getActualResponse(), 0, 32767));
                CellStyle style = wb.createCellStyle();
                Cell cellResult = row.createCell(10);
                if(testCase[i].getResult().equals("PASSED")) {
                    // Setting Background color
                    style.setFillForegroundColor(IndexedColors.GREEN.getIndex()); //Background màu xanh
                    style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
                    cellResult.setCellStyle(style);
                    cellResult.setCellValue(testCase[i].getResult());

                }else{
                    statusColorTab++;
                    // Setting Foreground Color
                    style = wb.createCellStyle();
                    style.setFillForegroundColor(IndexedColors.RED.getIndex()); //Background màu đỏ
                    style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
                    cellResult.setCellStyle(style);
                    cellResult.setCellValue(testCase[i].getResult());
                }
            }
            if(statusColorTab >0){
                Color red = new Color(255,0,0); //Màu đỏ
                sheet.setTabColor(new XSSFColor( red, new DefaultIndexedColorMap()));
            }else{
                Color green = new Color(20, 230, 18); //Màu Xanh
                sheet.setTabColor(new XSSFColor( green, new DefaultIndexedColorMap()));
            }
            wb.write(fileOutputStream);
            fileOutputStream.close();
            wb.close();
            System.out.println("Đã ghi file thành công!");
        } catch (FileNotFoundException e) {
           // e.printStackTrace();
            System.out.println("File output đang được mở nên không thể ghi đè. Vui lòng đóng file bạn nhé!");
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Không tìm thấy địa chỉ");
        }
    }
    /*private static void copySheets(XSSFWorkbook newWorkbook, XSSFSheet newSheet, XSSFSheet sheet, boolean copyStyle){
        int newRownumber = newSheet.getLastRowNum();
        int maxColumnNum = 0;
        Map<Integer, XSSFCellStyle> styleMap = (copyStyle) ? new HashMap<Integer, XSSFCellStyle>() : null;

        for (int i = sheet.getFirstRowNum(); i <= sheet.getLastRowNum(); i++) {
            XSSFRow srcRow = sheet.getRow(i);
            XSSFRow destRow = newSheet.createRow(i + newRownumber);
            if (srcRow != null) {
                copyRow(newWorkbook, sheet, newSheet, srcRow, destRow, styleMap);
                if (srcRow.getLastCellNum() > maxColumnNum) {
                    maxColumnNum = srcRow.getLastCellNum();
                }
            }
        }
        for (int i = 0; i <= maxColumnNum; i++) {
            newSheet.setColumnWidth(i, sheet.getColumnWidth(i));
        }
    }*/
    public void ghiFileExcelCon(ArrayList<TestCase> testCase, String refs, String diaChiFileOutput, int statusColorTab) {
        try {
            FileOutputStream fileOutputStream = new FileOutputStream(diaChiFileOutput);
            XSSFRow row = null;
            XSSFSheet sheetCon = null;
            //clone từ sheet cũ qua
            sheetCon = wb.cloneSheet(wb.getSheetIndex(sheet),sheet.getSheetName()+"_"+refs);

            //Clear data chỉ chừa là header
            for(int i=0;i<sheetCon.getLastRowNum();i++){
                row = sheetCon.createRow((short) i + 1);
            }
            int stt =1;
            for (int i = 0; i < testCase.size(); i++) {
                //Tạo hàng thứ 0 sử dụng phương thức
                row = sheetCon.createRow((short) i + 1);
                row.createCell(0).setCellValue(testCase.get(i).getRefs()+"_"+stt);
                row.createCell(1).setCellValue(testCase.get(i).getDescription());
                row.createCell(2).setCellValue(testCase.get(i).getEndPoint());
                row.createCell(3).setCellValue(testCase.get(i).getMethod());
                row.createCell(4).setCellValue(testCase.get(i).getHeader());
                row.createCell(5).setCellValue(testCase.get(i).getToken());
                row.createCell(6).setCellValue(testCase.get(i).getBody());
                row.createCell(7).setCellValue(testCase.get(i).getStatusCode());
                row.createCell(8).setCellValue(testCase.get(i).getExpectedResult());
                row.createCell(9).setCellValue(StringUtils.substring(testCase.get(i).getActualResponse(), 0, 32767));
                CellStyle style = wb.createCellStyle();
                Cell cellResult = row.createCell(10);
                if(testCase.get(i).getResult().equals("PASSED")) {
                    // Setting Background color
                    style.setFillForegroundColor(IndexedColors.GREEN.getIndex()); //Background màu xanh
                    style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
                    cellResult.setCellStyle(style);
                    cellResult.setCellValue(testCase.get(i).getResult());

                }else{
                    // Setting Foreground Color
                    style = wb.createCellStyle();
                    style.setFillForegroundColor(IndexedColors.RED.getIndex()); //Background màu đỏ
                    style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
                    cellResult.setCellStyle(style);
                    cellResult.setCellValue(testCase.get(i).getResult());
                }
                stt++;
            }
            if(statusColorTab >0){
                Color red = new Color(255,0,0); //Màu đỏ
                sheetCon.setTabColor(new XSSFColor( red, new DefaultIndexedColorMap()));
            }else{
                Color green = new Color(20, 230, 18); //Màu Xanh
                sheetCon.setTabColor(new XSSFColor( green, new DefaultIndexedColorMap()));
            }
            wb.write(fileOutputStream);
            System.out.println("Đã ghi file thành công!");
        } catch (FileNotFoundException e) {
            // e.printStackTrace();
            System.out.println("File output đang được mở nên không thể ghi đè. Vui lòng đóng file bạn nhé!");
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Không tìm thấy địa chỉ");
        }

    }


    public boolean hamKiemTraKetQua(String response, TestCase testcase) {
        // Nếu status code khác rỗng, kiểm tra status code
        boolean result = true;
        int temp = 0;
        if(!testcase.getStatusCode().equals("")){
            message = "";
            Object objResponse = Configuration.defaultConfiguration().jsonProvider().parse(response);
            int i_check = 0;
            //Kiểm tra trường hợp kết quả trả ra có nhiều biến.
            if(testcase.getExpectedResult().contains(";")){
                String[] arrKeyVerify = testcase.getExpectedResult().split(";");
                for(int i = 0;i<arrKeyVerify.length; i++){
                    i_check = hamSoSanhTheoToanTu(arrKeyVerify[i], objResponse);
                    if(i_check==0){
                        System.out.println("Kết quả này đúng rồi!");
                    }else{
                        temp = temp + i_check;
                        System.out.println("Kết quả này SAI rồi.........!");
                    }
                }
            }else{//Trường hợp trả ra có 1 biến
                temp = hamSoSanhTheoToanTu(testcase.getExpectedResult(), objResponse);
            }
        } else {// Nếu status code là rỗng thì lấy nguyên response so sánh với expected result
            return testcase.getExpectedResult().equalsIgnoreCase(response);
        }
        if(temp>0){
            result = false;
        }
        return result;

    }
    public ArrayList<DataParam> checkParamCuaTestCase(TestCase testcase, ArrayList<ResultTestCase> resultTestCases,int stt){
        ArrayList<DataParam> dataList = new ArrayList<DataParam>();
        int i = 0;
        DataParam dataParam = null;
        if(testcase.getEndPoint().contains("TESTCASE")){
            // lấy param
            dataParam = hamLayParam(testcase.getEndPoint(),resultTestCases,stt,"EndPoint");
            if(dataParam!= null){
                dataList.add(dataParam);
                i++;
            }

        }
        if(testcase.getHeader().contains("TESTCASE")){
            // lấy param
            dataParam = hamLayParam(testcase.getHeader(),resultTestCases,stt,"Header");
            if(dataParam!= null){
                dataList.add(dataParam);
                i++;
            }


        }
        if(testcase.getBody().contains("TESTCASE")){
            // lấy param
            dataParam= hamLayParamBody(testcase.getBody(),resultTestCases,stt,"Body");
            if(dataParam!= null){
                dataList.add(dataParam);
                i++;
            }
        }
        if(testcase.getExpectedResult().contains("TESTCASE")){
            // lấy param
            dataParam = hamLayParam(testcase.getExpectedResult(),resultTestCases,stt,"ExpectedResult");
            if(dataParam!= null){
                dataList.add(dataParam);
                i++;
            }
        }
        if(i==0){
            System.out.println("Param trong test case chưa được chạy");
            return null;
        }
        return dataList;
    }

    public DataParam hamLayParam(String headerTestCase, ArrayList<ResultTestCase> resultTestCases, int stt,String viTri){
        DataParam dataParam = new DataParam();
        dataParam.setViTri(viTri);

        int temp = 0;
        if(resultTestCases!=null) {
            for (int i = 0; i < stt; i++) {
                if (headerTestCase.contains(resultTestCases.get(i).getTestCase().getRefs())) { // Trường hợp tìm thấy test case
                    temp++;
                    if (headerTestCase.contains("ALLLIST")) {//Nếu có truyền text ALLLIST
                        //Set tên test case
                        dataParam.setRefs(resultTestCases.get(i).getTestCase().getRefs());
                        //Set vị trí của điểm đặt param (Header, Body, Endpoint hay Expected
                        dataParam.setViTri(viTri);
                        //Cắt đoạn trong dấu { }
                        String param = headerTestCase.substring(headerTestCase.lastIndexOf("{") + 1, headerTestCase.lastIndexOf("}"));
                        //Chia đoạn param và value
                        String key = param.substring(param.lastIndexOf(":") + 1);
                        //Chia đoạn trước của param để lấy alllist
                        String head = key.substring(0, key.lastIndexOf("[ALLLIST"));
                        //Chuyển đổi response thành dạng object
                        Object objResponse = Configuration.defaultConfiguration().jsonProvider().parse(resultTestCases.get(i).getResponse());
                        if (objResponse != null) {
                            String list = null;
                            //Lấy list data nhỏ của AllList
                            list = JsonPath.read(objResponse, head).toString();
                            JSONParser jsonParser = new JSONParser();
                            try {
                                JSONArray jsonArray = (JSONArray) jsonParser.parse(list);
                                if (jsonArray != null) {
                                    ArrayList<String> dataTemp = new ArrayList<>();
                                    for (int j = 0; j < jsonArray.size(); j++) {//Vòng lặp của list nhỏ ALLLIST
                                        //Lấy từng key
                                        String keyTemp = key.replaceAll("ALLLIST", String.valueOf(j));
                                        //Lấy từng value
                                        String valueTemp = readValueByJsonPath(objResponse, keyTemp);
                                        System.out.println("Kết quả: " + keyTemp + " : " + valueTemp);
                                        String ketQua = headerTestCase.substring(0, headerTestCase.lastIndexOf("{"));
                                        ketQua = ketQua + valueTemp;
                                        //Lấy kết quả chuỗi sau khi đã thay value
                                        ketQua = ketQua + headerTestCase.substring(headerTestCase.lastIndexOf("}") + 1);
                                        dataTemp.add(ketQua);
                                    }
                                    //Lưu lại kết quả
                                    dataParam.setDataList(dataTemp);

                                }
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                        }

                    } else {//Trường hợp param truyền vào ko có all list
                        String param = headerTestCase.substring(headerTestCase.lastIndexOf("{") + 1, headerTestCase.lastIndexOf("}"));
                        String key = param.substring(param.lastIndexOf(":") + 1);
                        dataParam.setRefs(resultTestCases.get(i).getTestCase().getRefs());
                        dataParam.setParam(key);
                        Object objResponse = Configuration.defaultConfiguration().jsonProvider().parse(resultTestCases.get(i).getResponse());
                        String result = readValueByJsonPath(objResponse, key);
                        dataParam.setValue(result);
                        String ketQua = headerTestCase.substring(0, headerTestCase.lastIndexOf("{"));
                        ketQua = ketQua + result;
                        ketQua = ketQua + headerTestCase.substring(headerTestCase.lastIndexOf("}") + 1);
                        dataParam.setData(ketQua);
                        dataParam.setViTri(viTri);
                        ArrayList<String> tempKetQua = new ArrayList<>();
                        tempKetQua.add(ketQua);
                        dataParam.setDataList(tempKetQua);
                    }
                }
            }
            if (temp > 0) {//Trường hợp tìm được dữ liệu
                dataParam.setKetqua(true);
                return dataParam;
            }
            else{//Không tìm thấy param
                dataParam.setKetqua(false);
                return dataParam;
            }
        }
        return null;
    }
    public DataParam hamLayParamBody(String headerTestCase, ArrayList<ResultTestCase> resultTestCases, int stt,String viTri){
        //Object objResponse = Configuration.defaultConfiguration().jsonProvider().parse(headerTestCase);
        DataParam dataParam = new DataParam();
        int temp = 0;
        for(int i=0;i<stt;i++){
            if(headerTestCase.contains(resultTestCases.get(i).getTestCase().getRefs())) { // Trường hợp tìm thấy test case
                temp++;
                if (headerTestCase.contains("ALLLIST")) {//Nếu có truyền text ALLLIST
                    //Set tên test case
                    dataParam.setRefs(resultTestCases.get(i).getTestCase().getRefs());
                    //Set vị trí của điểm đặt param (Header, Body, Endpoint hay Expected
                    dataParam.setViTri(viTri);
                    //Cắt đoạn trong dấu { }
                    String param = headerTestCase.substring(headerTestCase.lastIndexOf("{$") + 2, headerTestCase.lastIndexOf("$}"));
                    //Chia đoạn param và value
                    String key = param.substring(param.lastIndexOf(":") + 1);
                    //Chia đoạn trước của param để lấy alllist
                    String head = key.substring(0, key.lastIndexOf("[ALLLIST"));
                    //Chuyển đổi response thành dạng object
                    Object objResponse = Configuration.defaultConfiguration().jsonProvider().parse(resultTestCases.get(i).getResponse());
                    if (objResponse != null) {
                        String list = null;
                        //Lấy list data nhỏ của AllList
                        list = JsonPath.read(objResponse, head).toString();
                        JSONParser jsonParser = new JSONParser();
                        try {
                            JSONArray jsonArray = (JSONArray) jsonParser.parse(list);
                            if (jsonArray != null) {
                                ArrayList<String> dataTemp = new ArrayList<>();
                                for (int j = 0; j < jsonArray.size(); j++) {//Vòng lặp của list nhỏ ALLLIST
                                    //Lấy từng key
                                    String keyTemp = key.replaceAll("ALLLIST", String.valueOf(j));
                                    //Lấy từng value
                                    String valueTemp = readValueByJsonPath(objResponse, keyTemp);
                                    System.out.println("Kết quả: " + keyTemp + " : " + valueTemp);
                                    String ketQua = headerTestCase.substring(0, headerTestCase.lastIndexOf("{$"));
                                    ketQua = ketQua + valueTemp;
                                    //Lấy kết quả chuỗi sau khi đã thay value
                                    ketQua = ketQua + headerTestCase.substring(headerTestCase.lastIndexOf("$}") + 2);
                                    dataTemp.add(ketQua);
                                }
                                //Lưu lại kết quả
                                dataParam.setDataList(dataTemp);

                            }
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                    }

                } else {
                    String param = headerTestCase.substring(headerTestCase.lastIndexOf("{$") + 2, headerTestCase.lastIndexOf("$}"));
                    String key = param.substring(param.lastIndexOf(":") + 1);
                    dataParam.setRefs(resultTestCases.get(i).getTestCase().getRefs());
                    dataParam.setParam(key);
                    Object objResponse = Configuration.defaultConfiguration().jsonProvider().parse(resultTestCases.get(i).getResponse());
                    String result = readValueByJsonPath(objResponse, key);
                    dataParam.setValue(result);
                    String ketQua = headerTestCase.substring(0, headerTestCase.lastIndexOf("{$"));
                    ketQua = ketQua + result;
                    ketQua = ketQua + headerTestCase.substring(headerTestCase.lastIndexOf("$}") + 2);
                    dataParam.setData(ketQua);
                    dataParam.setViTri(viTri);
                    ArrayList<String> tempKetQua = new ArrayList<>();
                    tempKetQua.add(ketQua);
                    dataParam.setDataList(tempKetQua);

                }
            }

        }
        if(temp>0){//Trường hợp tìm được dữ liệu
            dataParam.setKetqua(true);
            return dataParam;
        } else{//Không tìm thấy param
            dataParam.setKetqua(false);
            return dataParam;
        }

    }
    public DataParam hamLayParamBodyAllList(String headerTestCase, ArrayList<ResultTestCase> resultTestCases, int stt,String viTri){
        //Object objResponse = Configuration.defaultConfiguration().jsonProvider().parse(headerTestCase);


        DataParam dataParam = new DataParam();
        int temp = 0;
        for(int i=0;i<stt;i++){
            if(headerTestCase.contains(resultTestCases.get(i).getTestCase().getRefs())){ // Trường hợp tìm thấy test case
                temp++;
                String param = headerTestCase.substring(headerTestCase.lastIndexOf("{$")+2,headerTestCase.lastIndexOf("$}"));
                String key =param.substring(param.lastIndexOf(":")+1);
                dataParam.setRefs(resultTestCases.get(i).getTestCase().getRefs());
                dataParam.setParam(key);
                Object objResponse = Configuration.defaultConfiguration().jsonProvider().parse(resultTestCases.get(i).getResponse());
                String result = readValueByJsonPath(objResponse,key);
                dataParam.setValue(result);
                String ketQua = headerTestCase.substring(0,headerTestCase.lastIndexOf("{$"));
                ketQua = ketQua +result;
                ketQua = ketQua + headerTestCase.substring(headerTestCase.lastIndexOf("$}")+2);
                dataParam.setData(ketQua);
                dataParam.setViTri(viTri);

            }

        }
        if(temp>0){//Trường hợp tìm được dữ liệu
            return dataParam;
        }

        return null;
    }
    public ArrayList<DataParam> hamCheckParam(TestCase testcase, ArrayList<ResultTestCase> resultTestCases, int stt ) {
        ArrayList<DataParam> dataList = new ArrayList<DataParam>();
        dataList= checkParamCuaTestCase(testcase,resultTestCases,stt);
     return dataList;
    }

    public int hamSoSanhTheoToanTu(String keyVerify, Object objJson){
        int icheck = 0;
        if(keyVerify.contains("==")){
            icheck = hamSoSanh("==",keyVerify,objJson);
        }else if (keyVerify.contains("!=")){
            icheck = hamSoSanh("!=",keyVerify,objJson);
        }else if (keyVerify.contains(">=")) {
            icheck = hamSoSanh(">=", keyVerify, objJson);
        }else if (keyVerify.contains("<=")) {
            icheck = hamSoSanh("<=", keyVerify, objJson);
        }else if (keyVerify.contains(">")) {
            icheck = hamSoSanh(">", keyVerify, objJson);
        }else if (keyVerify.contains("<<")) {
            icheck = hamSoSanh("<<", keyVerify, objJson);
        }else if (keyVerify.contains("<")) {
            icheck = hamSoSanh("<", keyVerify, objJson);
        }else {
            System.out.println("Không tìm thấy toán tử so sánh");
            icheck = 1;
        }
        return icheck;

    }
    public int hamSoSanh (String toanTu, String keyVerify, Object objJson){
        int icheck = 0;
        String[] arrExpectResult = keyVerify.split(toanTu);
        String key = arrExpectResult[0].trim();
        String expectValue = arrExpectResult[1];
        if (expectValue.contains("TESTCASE")){

        }
        String actualValue = readValueByJsonPath(objJson,key);
        System.out.println("Key: "+key);
        String toanTuTam ="";
        if(!toanTu.equals("==")){
            toanTuTam = toanTu;
        }
        System.out.println("Expect: "+ toanTuTam + expectValue);
        System.out.println("Actual: "+ actualValue);
        switch (toanTu) {
            case "==": {
                if (expectValue.equals(actualValue))
                    icheck = 0;
                else {
                    icheck = 1;
                    message = key + ": " + actualValue + " - Kết quả trả ra khác với giá trị mong muốn là: "+ toanTuTam  + expectValue;
                    System.out.println(message);
                }
            }
            break;
            case "!=": {
                if (!expectValue.equals(actualValue))
                    icheck = 0;
                else {
                    icheck = 1;
                    message = key + ": " + actualValue + " - Kết quả trả ra khác với giá trị mong muốn là: " + toanTuTam + expectValue;
                    System.out.println(message);
                }
            }
            break;
            case ">=": {
                if (Integer.valueOf(actualValue) >= Integer.valueOf(expectValue) )
                    icheck = 0;
                else {
                    icheck = 1;
                    message = key + ": " + actualValue + " - Kết quả trả ra khác với giá trị mong muốn là: " + toanTuTam + expectValue;
                    System.out.println(message);
                }
            }
            break;
            case "<=": {
                if (Integer.valueOf(actualValue) <= Integer.valueOf(expectValue) )
                    icheck = 0;
                else {
                    icheck = 1;
                    message = key + ": " + actualValue + " - Kết quả trả ra khác với giá trị mong muốn là: " + toanTuTam + expectValue;
                    System.out.println(message);
                }
            }
            break;
            case ">": {
                if (Integer.valueOf(actualValue) > Integer.valueOf(expectValue) )
                    icheck = 0;
                else {
                    icheck = 1;
                    message = key + ": " + actualValue + " - Kết quả trả ra khác với giá trị mong muốn là: " + toanTuTam + expectValue;
                    System.out.println(message);
                }
            }
            break;
            case "<": {
                if (Integer.valueOf(actualValue) < Integer.valueOf(expectValue) )
                    icheck = 0;
                else {
                    icheck = 1;
                    message = key + ": " + actualValue + " - Kết quả trả ra khác với giá trị mong muốn là: " + toanTuTam + expectValue;
                    System.out.println(message);
                }
            }
            break;
            case "<<": {
                if (expectValue.contains(actualValue))
                    icheck = 0;
                else {
                    icheck = 1;
                    message = key + ": " + actualValue + " - Kết quả trả ra không tồn tại trong giá trị mong muốn: " + expectValue;
                    System.out.println(message);
                }
            }
            break;
            default:
                break;

        }
        return icheck;
    }

    //Hàm lấy giá trị từ json
    public String readValueByJsonPath(Object json, String key){
        try{
            return JsonPath.read(json,key).toString();
        }catch (PathNotFoundException e){
           // e.printStackTrace();
            System.out.println("Không tìm thấy param " + key);
            return null;
        }
    }


    public void sendNotificationHangsOut(ITestContext testContext, String resultPath) {
        System.out.println("-- Start to reporting --");
        String suiteName = testContext.getCurrentXmlTest().getSuite().getName();
        //String currentTime = ActionGen.getCurrentTimeByTimezoneOffset(Integer.valueOf(Configurations.timezoneOffset), "yyyy-MM-dd HH:mm:ss");
        Date now = new Date();
        System.out.println("Thời gian hiện tại là: " +now);
        //Upload file result to Drive
        String fileUrl = "https://drive.google.com/open?id=" + GoogleDrive.uploadExcelFile(resultPath);
//        GoogleDrive.setExcelPermission();

        //Send message to GG Chat
        StringBuilder messageBuilder = new StringBuilder();
        //messageBuilder.append("*API Seller Automation - " + BaseTest.environment.toUpperCase() + " - " + suiteName + " - " + currentTime + "*\n");
        messageBuilder.append("*API ZMA Sendo Farm - " + suiteName + " - " + now + "*\n");
        messageBuilder.append("- Passed case: " + BaseTest.totalPassCase+ "\n");
        messageBuilder.append("- Failed case: " + BaseTest.totalFailCase + "\n");
        messageBuilder.append("Detail report: " + "<" + fileUrl + "|Click here>\n");
        messageBuilder.append("=======================================================");

        com.google.api.services.chat.v1.model.Message hangoutsMessage = new com.google.api.services.chat.v1.model.Message();
        hangoutsMessage.setText(messageBuilder.toString());
        if(System.getProperty("os.name").startsWith("Linux"))
            GoogleHangouts.sendHangoutsMessage("AAAA3oiow_Q",hangoutsMessage);
        else
            GoogleHangouts.sendHangoutsMessage("AAAA3oiow_Q", hangoutsMessage); //Use for test noti thread
    }
}
