package utils;

public class Constants {
    public static String INPUT_PATH = System.getProperty("user.dir") + "/Input/";
    public static String OUTPUT_PATH = System.getProperty("user.dir") + "/Output/";
    public static String IMPORT_PATH = System.getProperty("user.dir") + "/import/";
    public static String IMAGE_PATH = System.getProperty("user.dir") + "src/test/java/Run/Input";
    public static String LOG_PATH = System.getProperty("user.dir") + "/logs/log4j/";
}