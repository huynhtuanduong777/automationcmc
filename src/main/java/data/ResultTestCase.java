package data;

public class ResultTestCase {
    public TestCase testCase = null;
    String response = null;

    public ResultTestCase(TestCase testCase, String response) {
        this.testCase = testCase;
        this.response = response;
    }

    public TestCase getTestCase() {
        return testCase;
    }

    public void setTestCase(TestCase testCase) {
        this.testCase = testCase;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }
}
