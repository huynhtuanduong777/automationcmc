package data;

public class TestCase {
    public String Refs = " ";
    public  String Description = " ";
    public String EndPoint = " ";
    public  String Method = " ";
    public  String Header = " ";
    public  String Token = " ";
    public  String Body = " ";
    public  String StatusCode = " ";
    public  String ExpectedResult = " ";
    public  String ActualResponse= " ";
    public  String Result= " ";



    public TestCase() {
    }

    public TestCase(String refs, String description, String endPoint, String method, String header, String token, String body, String statusCode, String expectedResult, String actualResponse, String result) {
        Refs = refs;
        Description = description;
        EndPoint = endPoint;
        Method = method;
        Header = header;
        Token = token;
        Body = body;
        StatusCode = statusCode;
        ExpectedResult = expectedResult;
        ActualResponse = actualResponse;
        Result = result;

    }

    public void setViTri(String keyWord, String dataValue){
    switch (keyWord) {
        case "EndPoint": {
            EndPoint = dataValue;
            break;
        }
        case "Header": {
            Header = dataValue;
            break;
        }
        case "Body": {
            Body = dataValue;
            break;
        }
        case "ExpectedResult": {
            ExpectedResult = dataValue;
            break;
        }
        default:
            break;

    }

}


    public String getRefs() {
        return Refs;
    }

    public void setRefs(String refs) {
        Refs = refs;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public String getEndPoint() {
        return EndPoint;
    }

    public void setEndPoint(String endPoint) {
        EndPoint = endPoint;
    }

    public String getMethod() {
        return Method;
    }

    public void setMethod(String method) {
        Method = method;
    }

    public String getHeader() {
        return Header;
    }

    public void setHeader(String header) {
        Header = header;
    }

    public String getToken() {
        return Token;
    }

    public void setToken(String token) {
        Token = token;
    }

    public String getBody() {
        return Body;
    }

    public void setBody(String body) {
        Body = body;
    }

    public String getStatusCode() {
        return StatusCode;
    }

    public void setStatusCode(String statusCode) {
        StatusCode = statusCode;
    }

    public String getExpectedResult() {
        return ExpectedResult;
    }

    public void setExpectedResult(String expectedResult) {
        ExpectedResult = expectedResult;
    }

    public String getActualResponse() {
        return ActualResponse;
    }

    public void setActualResponse(String actualResponse) {
        ActualResponse = actualResponse;
    }

    public String getResult() {
        return Result;
    }

    public void setResult(String result) {
        Result = result;
    }

    public void xuatTestCase(){
        System.out.println("Ref: "+Refs);
        System.out.println("Desc: "+Description);
        System.out.println("Endpoint: "+EndPoint);
        System.out.println("Header: "+Header);
        System.out.println("Body: "+Body);
        System.out.println("Exepected: "+ExpectedResult);
        System.out.println("Method: "+Method);
        System.out.println("ActualResponse: "+ActualResponse);
        System.out.println("Result: "+Result);
        System.out.println("Token: "+Token);

    }

}
