package data;

import java.util.ArrayList;

public class DataParam {
    public String refs = " ";
    public  String param = " ";
    public String value = " ";
    public  String viTri = " ";
    public  String data = " ";
    public Boolean ketqua = false;
    public ArrayList<String> dataList= null;
    public DataParam() {
    }
    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public ArrayList<String> getDataList() {
        return dataList;
    }

    public void setDataList(ArrayList<String> dataList) {
        this.dataList = dataList;
    }

    public DataParam(String refs, String param, String value, String viTri, String data, ArrayList<String> dataList) {
        this.refs = refs;
        this.param = param;
        this.value = value;
        this.viTri = viTri;
        this.data = data;
        this.dataList = dataList;
    }

    public DataParam(String refs, String param, String value, String viTri, String data) {
        this.refs = refs;
        this.param = param;
        this.value = value;
        this.viTri = viTri;
        this.data = data;
    }

    public Boolean getKetqua() {
        return ketqua;
    }

    public void setKetqua(Boolean ketqua) {
        this.ketqua = ketqua;
    }

    public DataParam(String refs, String param, String value, String viTri, String data, Boolean ketqua, ArrayList<String> dataList) {
        this.refs = refs;
        this.param = param;
        this.value = value;
        this.viTri = viTri;
        this.data = data;
        this.ketqua = ketqua;
        this.dataList = dataList;
    }

    public String getRefs() {
        return refs;
    }

    public void setRefs(String refs) {
        this.refs = refs;
    }

    public String getParam() {
        return param;
    }

    public void setParam(String param) {
        this.param = param;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getViTri() {
        return viTri;
    }

    public void setViTri(String viTri) {
        this.viTri = viTri;
    }
}
